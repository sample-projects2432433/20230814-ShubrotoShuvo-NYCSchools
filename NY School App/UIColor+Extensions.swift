//
//  UIColor+Extensions.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/10/23.
//

import Foundation
import UIKit

extension UIColor {
    static let bg: UIColor = UIColor(red: 82/255.0, green: 78/255.0, blue: 191/255.0, alpha: 1)
    static let lightBg: UIColor = UIColor(red: 82/255.0, green: 78/255.0, blue: 191/255.0, alpha: 0.8)
    static let cardBg: UIColor = UIColor(red: 155/255.0, green: 151/255.0, blue: 255/255.0, alpha: 1)
    static let cardText: UIColor = .white

    
}
