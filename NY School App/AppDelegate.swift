//
//  AppDelegate.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation
import UIKit

@main
final class AppDelegate: NSObject, UIApplicationDelegate {
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {
        UINavigationBar.appearance().tintColor = .white
        let backButtonAppearance = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self])
        backButtonAppearance.setTitleTextAttributes([.font: UIFont.systemFont(ofSize: 0.1), .foregroundColor: UIColor.clear], for: .normal)
        backButtonAppearance.setTitleTextAttributes([.font: UIFont.systemFont(ofSize: 0.1), .foregroundColor: UIColor.clear], for: .highlighted)
        
        return true
    }
    
    func application(
        _ application: UIApplication,
        configurationForConnecting connectingSceneSession: UISceneSession,
        options: UIScene.ConnectionOptions
    ) -> UISceneConfiguration {
        let sessionRole = connectingSceneSession.role
        let sceneConfig = UISceneConfiguration(name: nil, sessionRole: sessionRole)
        sceneConfig.delegateClass = SceneDelegate.self
        return sceneConfig
    }
}
