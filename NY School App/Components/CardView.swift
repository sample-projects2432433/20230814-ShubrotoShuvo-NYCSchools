//
//  CardView.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/11/23.
//

import Foundation
import SwiftUI

struct Card<Content: View>: View {
    var content: Content
    
    init(content: @escaping () -> Content) {
        self.content = content()
    }
    
    var body: some View {
        VStack(spacing: 0) {
            content
        }
        .frame(maxWidth: .infinity)
        .padding(15)
        .background(Color(.cardBg))
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.3), radius: 5, x: 0, y: 2)
    }
}
