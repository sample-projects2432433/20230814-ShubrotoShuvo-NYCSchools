//
//  MapView.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/11/23.
//

import SwiftUI
import MapKit

struct IdentifiableCoordinate: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
}

struct MapView: View {
    @State private var region: MKCoordinateRegion

    init(latitude: Double, longitude: Double) {
        _region = State(initialValue: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)))
    }

    var body: some View {
        Map(coordinateRegion: $region, annotationItems: [IdentifiableCoordinate(coordinate: region.center)]) { identifiableCoordinate in
            MapMarker(coordinate: identifiableCoordinate.coordinate, tint: .red)
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(latitude: 37.7749, longitude: -122.4194) // For San Francisco
    }
}


