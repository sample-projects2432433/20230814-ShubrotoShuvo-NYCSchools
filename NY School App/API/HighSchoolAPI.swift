//
//  HighSchoolAPI.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation

protocol HighSchoolAPI {
    func getSchoolList() async throws -> [HighSchoolModel]
    func getSATScore() async throws -> [SatScoreModel]
}

class HighSchoolAPIImp: HighSchoolAPI {
    private let urlSession: URLSessionHelper
    
    init(urlSession: URLSessionHelper = URLSessionHelperImp()) {
        self.urlSession = urlSession
    }
    
    func getSchoolList() async throws -> [HighSchoolModel] {
        let endpoint = Endpoint.baseURL + Endpoint.HightSchool.highSchool
        let result = try await urlSession.send(endpoint, method: .GET, model: [HighSchoolModel].self)
        return result
    }
    
    func getSATScore() async throws -> [SatScoreModel] {
        let endpoint = Endpoint.baseURL + Endpoint.HightSchool.satScores
        let result = try await urlSession.send(endpoint, method: .GET, model: [SatScoreModel].self)
        return result
    }
}

struct HighSchoolModel: Codable {
    var dbn: String?
    var name: String?
    var overview: String?
    var phone: String?
    var email: String?
    var website: String?
    var totalStudents: Int?
    var city: String?
    var latitude: Double?
    var longitude: Double?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case phone = "phone_number"
        case email = "school_email"
        case website
        case totalStudents = "total_students"
        case city
        case latitude
        case longitude
    }
    
    init(dbn: String, name: String, overview: String, phone: String, email: String, website: String, totalStudents: Int, city: String) {
        self.dbn = dbn
        self.name = name
        self.overview = overview
        self.phone = phone
        self.email = email
        self.website = website
        self.totalStudents = totalStudents
        self.city = city
    }
    
    init(dbn: String) {
        self.dbn = dbn
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dbn = try? container.decode(String.self, forKey: .dbn)
        name = try? container.decode(String.self, forKey: .name)
        overview = try? container.decode(String.self, forKey: .overview)
        phone = try? container.decode(String.self, forKey: .phone)
        email = try? container.decode(String.self, forKey: .email)
        website = "https://\((try? container.decode(String.self, forKey: .website)) ?? "")"
        totalStudents = Int((try? container.decode(String.self, forKey: .totalStudents)) ?? "") ?? 0
        city = try? container.decode(String.self, forKey: .city)
        latitude = Double((try? container.decode(String.self, forKey: .latitude)) ?? "")
        longitude = Double((try? container.decode(String.self, forKey: .longitude)) ?? "")
    }
}

struct SatScoreModel: Codable {
    var dbn: String?
    var satTakers: Int?
    var readingScore: Int?
    var mathScore: Int?
    var writingScore: Int?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case satTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
    
    init(dbn: String) {
        self.dbn = dbn
    }
    
    init(dbn: String, satTakers: Int, readingScore: Int, mathScore: Int, writingScore: Int) {
        self.dbn = dbn
        self.satTakers = satTakers
        self.readingScore = readingScore
        self.mathScore = mathScore
        self.writingScore = writingScore
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        dbn = try? container.decode(String.self, forKey: .dbn)
        satTakers = Int((try? container.decode(String.self, forKey: .satTakers)) ?? "")
        readingScore = Int((try? container.decode(String.self, forKey: .readingScore)) ?? "")
        mathScore = Int((try? container.decode(String.self, forKey: .mathScore)) ?? "")
        writingScore = Int((try? container.decode(String.self, forKey: .writingScore)) ?? "")
    }
}
