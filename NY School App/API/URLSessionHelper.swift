//
//  URLSessionHelper.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation

enum Method: String {
    case GET, POST, PUT, DELETE
}

protocol URLSessionHelper {
    func send<T: Codable>(_ url: String, method: Method, model: T.Type) async throws -> T
}

class URLSessionHelperImp: URLSessionHelper {
    func send<T: Codable>(_ url: String, method: Method, model: T.Type) async throws -> T {
        guard let url = URL(string: url) else {
            throw URLError(.badURL)
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        let (data, response) = try await URLSession.shared.data(for: request)
        guard let response = response as? HTTPURLResponse,  response.statusCode == 200 else {
            throw URLError(.badServerResponse)
        }
        let result = try JSONDecoder().decode(model, from: data)
        return result
        
    }
    
    
}

