//
//  Endpoints.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation

struct Endpoint {
    static var baseURL = "https://data.cityofnewyork.us/resource"
    
    struct HightSchool {
        static var highSchool = "/s3k6-pzi2.json"
        static var satScores = "/f9bf-2cp4.json"
    }
}
