//
//  HomeViewModel.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation
import Combine
import UIKit
import SwiftUI

class HomeViewModel {
    private let highSchoolAPI: HighSchoolAPI
    
    var highSchoolDataSubject: PassthroughSubject<[HighSchoolData], Never>
    var highSchoolDataSubjectError: PassthroughSubject<Error, Never>
    
    // Refactor to use Factory for managing dependency
    init(highSchoolApi: HighSchoolAPI = HighSchoolAPIImp()) {
        self.highSchoolAPI = highSchoolApi
        self.highSchoolDataSubject = .init()
        self.highSchoolDataSubjectError = .init()
    }
    
    func getHighSchoolData() {
        Task {
            do {
                async let highSchoolAPITask = highSchoolAPI.getSchoolList()
                async let satScoreAPITask = highSchoolAPI.getSATScore()
                
                let highSchoolModels = try await highSchoolAPITask
                let satScoreModels = try await satScoreAPITask
                let highSchoolData = await Task.detached {[unowned self] in
                    self.processHighSchoolData(from: highSchoolModels, and: satScoreModels)
                }.value
                
                await sendSuccessData(highSchoolData)
            } catch {
                await sendFailedData(error)
            }
        }
    }
    
    private func processHighSchoolData(from schoolModels: [HighSchoolModel], and satScoreModels: [SatScoreModel]) -> [HighSchoolData] {
        var highSchoolDataArray: [HighSchoolData] = []
        let satScoresDict = Dictionary(uniqueKeysWithValues: satScoreModels.map { ($0.dbn, $0) })
        for schoolModel in schoolModels {
            if let dbn = schoolModel.dbn, let satScoreModel = satScoresDict[dbn] {
                let highSchoolData = HighSchoolData(schoolInfo: schoolModel, satInfo: satScoreModel)
                highSchoolDataArray.append(highSchoolData)
            }
        }
        return highSchoolDataArray
    }
    
    @MainActor
    private func sendSuccessData(_ model: [HighSchoolData]) {
        highSchoolDataSubject.send(model)
    }
    
    @MainActor
    private func sendFailedData(_ error: Error) {
        highSchoolDataSubjectError.send(error)
    }
    
    //TODO: - Moving all the routing to a separate class like cordinators to handle routing and routing logics
    func showSchoolDetail(for data: HighSchoolData, _ context: UIViewController) {
        let viewModel = HighSchoolDetailView.ViewModel(highSchoolData: data)
        let hostingController = UIHostingController(rootView: HighSchoolDetailView(viewModel: viewModel))
        context.navigationController?.pushViewController(hostingController, animated: true)
    }
}

// Later we can destruct this and get the data out from the models for more clear understanding
struct HighSchoolData {
    var schoolInfo: HighSchoolModel
    var satInfo: SatScoreModel
    
    static func previewData() -> HighSchoolData {
        let school = HighSchoolModel(dbn: "111111", name: "High School Testing High school", overview: "This is a great school and pretty amazing school. They have great students and the best students of all", phone: "111-111-1111", email: "test@gmail.com", website: "www.test.com", totalStudents: 300, city: "Manhattan")
        let sat = SatScoreModel(dbn: "111111", satTakers: 50, readingScore: 300, mathScore: 200, writingScore: 400)
        return HighSchoolData(schoolInfo: school, satInfo: sat)
    }
}
