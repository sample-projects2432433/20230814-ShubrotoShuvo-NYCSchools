//
//  HighSchoolListCell.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/9/23.
//

import Foundation
import UIKit
import SwiftUI


class HighSchoolListCell: UITableViewCell {
    private lazy var mainStack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [contentStackWrapper, barrier])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        return view
    }()
    
    private lazy var contentStackWrapper: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var contentStack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [title, phoneLabel, totalStudentLabel, infoStack])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.spacing = 5
        view.axis = .vertical
        return view
    }()

    
    private lazy var title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.textColor = .white
        return label
    }()
    
    private lazy var phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = .white
        return label
    }()
    
    private lazy var totalStudentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = .white
        return label
    }()
    
    private lazy var infoStack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [dbnLabel, locationLabel])
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dbnLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.textColor = .white
        return label
    }()
    
    private lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        label.textColor = .white
        return label
    }()
    
    private lazy var barrier: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()

    func configure(with data: HighSchoolData) {
        contentView.addSubview(mainStack)
        contentStackWrapper.addSubview(contentStack)
        
        contentStack.setCustomSpacing(35, after: totalStudentLabel)
        
        NSLayoutConstraint.activate([
            mainStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            mainStack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0),
            mainStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0),
            mainStack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0),
            
            barrier.heightAnchor.constraint(equalToConstant: 1),
            
            contentStack.topAnchor.constraint(equalTo: contentStackWrapper.topAnchor, constant: 20),
            contentStack.leftAnchor.constraint(equalTo: contentStackWrapper.leftAnchor, constant: 20),
            contentStack.bottomAnchor.constraint(equalTo: contentStackWrapper.bottomAnchor, constant: -20),
            contentStack.rightAnchor.constraint(equalTo: contentStackWrapper.rightAnchor, constant: -20),
        ])
        
        title.text = data.schoolInfo.name
        phoneLabel.text = "Phone: \(data.schoolInfo.phone ?? "")"
        totalStudentLabel.text = "Total Student: \(data.schoolInfo.totalStudents ?? 0)"
        dbnLabel.text = "DBN: \(data.schoolInfo.dbn ?? "")"
        locationLabel.text = "Location: \(data.schoolInfo.city ?? "")"
    }
    
    func setBGColor(index: Int) {
        contentView.backgroundColor = index % 2 == 0 ? .bg : .lightBg
    }
}
