//
//  HomeViewController.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation
import UIKit
import Combine
import Lottie

class HomeViewController: UIViewController {
    private let viewModel: HomeViewModel
    
    private var highSchoolList: [HighSchoolData] = []
    private var cancellable = Set<AnyCancellable>()
    
    lazy var schoolTableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorStyle = .none
        table.backgroundColor = .bg
        return table
    }()
    
    lazy var loaderAnimation: LottieAnimationView = {
        let view = LottieAnimationView(name: "loader")
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        view.loopMode = .loop
        return view
    }()
    
    //TODO: - Given more time this whole error can be in a separate view to make it more cleaner
    lazy var errorAnimation: LottieAnimationView = {
        let view = LottieAnimationView(name: "errorAnimation")
        view.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        view.loopMode = .loop
        return view
    }()
    
    lazy var errorText: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    lazy var tryAgainLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .cardBg
        label.text = "Try again?"
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.isUserInteractionEnabled = true
        return label
    }()
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // bindings
        viewModel.highSchoolDataSubject
            .sink {[weak self] highSchoolList in
                self?.shouldShowLoader(false)
                self?.showError(false, error: "")
                
                self?.highSchoolList = highSchoolList
                self?.schoolTableView.reloadData()
            }
            .store(in: &cancellable)
        
        viewModel.highSchoolDataSubjectError
            .sink {[weak self] error in
                self?.shouldShowLoader(false)
                self?.showError(true, error: "Opps looks like there is an issue")
            }
            .store(in: &cancellable)
        
        // Table View Config
        schoolTableView.dataSource = self
        schoolTableView.delegate = self
        schoolTableView.register(HighSchoolListCell.self, forCellReuseIdentifier: "HIGH_SCHOOL_LIST")
        
        schoolTableView.rowHeight = UITableView.automaticDimension
        schoolTableView.estimatedRowHeight = 500
        
        // UI Config
        setupUI()
        view.backgroundColor = .bg
        
        // API Call
        shouldShowLoader(true)
        viewModel.getHighSchoolData()
        
        tryAgainLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tryAgainTapped)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "NYC Schools"
        navigationController?.navigationBar.barTintColor = .bg
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    }
    
    @objc private func tryAgainTapped() {
        shouldShowLoader(true)
        showError(false, error: "")
        viewModel.getHighSchoolData()
    }
    
    private func setupUI () {
        view.addSubview(schoolTableView)
        
        NSLayoutConstraint.activate([
            schoolTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            schoolTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            schoolTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            schoolTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
        ])
    }
    
    private func shouldShowLoader(_ show: Bool) {
        if show {
            view.addSubview(loaderAnimation)
            loaderAnimation.play()
            loaderAnimation.center = view.center
        } else {
            loaderAnimation.removeFromSuperview()
        }
    }
    
    private func showError(_ show: Bool, error: String) {
        if show {
            view.addSubview(errorAnimation)
            view.addSubview(errorText)
            view.addSubview(tryAgainLabel)
            errorText.text = error
            errorAnimation.play()
            errorAnimation.center = view.center
            
            NSLayoutConstraint.activate([
                errorText.topAnchor.constraint(equalTo: errorAnimation.bottomAnchor, constant: 0),
                errorText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                tryAgainLabel.topAnchor.constraint(equalTo: errorText.bottomAnchor, constant: 10),
                tryAgainLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ])
        } else {
            errorAnimation.removeFromSuperview()
            errorText.removeFromSuperview()
            tryAgainLabel.removeFromSuperview()
        }
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        highSchoolList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HIGH_SCHOOL_LIST", for: indexPath) as! HighSchoolListCell
        cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let highSchool = highSchoolList[indexPath.row]
        cell.configure(with: highSchool)
        cell.setBGColor(index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.showSchoolDetail(for: highSchoolList[indexPath.row], self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        500
    }

}
