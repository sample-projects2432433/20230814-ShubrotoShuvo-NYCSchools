//
//  HighSchoolDetailView.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/11/23.
//

import SwiftUI

struct HighSchoolDetailView: View {
    @StateObject var viewModel: ViewModel
    
    var body: some View {
        ZStack {
            Color(.bg)
                .ignoresSafeArea()
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 0) {
                    headerView
                    
                    cardInfoView
                        .padding(.top, 20)
                    
                    Button(action: { viewModel.openWebView() }) {
                        HStack(spacing: 10) {
                            Image(systemName: "globe")
                                .font(.body)
                                .tint(.white)
                            Text("Go to the Website")
                                .font(.body)
                                .foregroundColor(.white)
                        }
                        .frame(maxWidth: .infinity)
                        .padding(10)
                        .background(Capsule().stroke(Color.white, lineWidth: 1))
                        .background(Color.clear)
                    }
                    .padding(.top, 10)
                    
                    totalStudentView
                        .padding(.top, 60)
                    
                    satTakerView
                        .padding(.top, 10)
                    
                    overviewView
                        .padding(.top, 30)
                    
                    satScoreView
                        .padding(.top, 30)
                    
                    locationView
                        .padding(.top, 30)
                    
                    Spacer()
                    
                }
            }
            .padding(.horizontal, 25)
        }
    }
    
    var headerView: some View {
        VStack(spacing: 0) {
            Text(viewModel.highSchoolData.schoolInfo.name ?? "")
                .font(.headline)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            Text("DBN: \(viewModel.highSchoolData.schoolInfo.dbn ?? "-")")
                .font(.subheadline)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 5)
        }
    }
    
    var cardInfoView: some View {
        Card {
            VStack(spacing: 10) {
                HStack {
                    Image(systemName: "building.2.fill")
                        .foregroundColor(Color(uiColor: .cardText))
                        .font(.body)
                        .fixedSize()
                        
                    Text(viewModel.highSchoolData.schoolInfo.city ?? "-")
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color(uiColor: .cardText))
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
                
                HStack {
                    Image(systemName: "tray.fill")
                        .foregroundColor(Color(uiColor: .cardText))
                        .font(.body)
                        .fixedSize()
                        
                    Text(viewModel.highSchoolData.schoolInfo.email ?? "-")
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color(uiColor: .cardText))
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
                
                HStack {
                    Image(systemName: "phone.fill")
                        .foregroundColor(Color(uiColor: .cardText))
                        .font(.body)
                        .fixedSize()
                        
                    Text(viewModel.highSchoolData.schoolInfo.phone ?? "-")
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color(uiColor: .cardText))
                        .frame(maxWidth: .infinity, alignment: .trailing)
                }
                
            }
        }
    }
    
    var totalStudentView: some View {
        HStack {
            Text("Total Students")
                .font(.headline)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
                .fixedSize()
                
            
            Text("\(viewModel.highSchoolData.schoolInfo.totalStudents ?? 0)")
                .font(.title2)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .trailing)
        }
    }
    
    var satTakerView: some View {
        HStack {
            Text("SAT Takers")
                .font(.headline)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
                .fixedSize()
                
            
            Text("\(viewModel.highSchoolData.satInfo.satTakers ?? 0)")
                .font(.title2)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .trailing)
        }
    }
    
    var overviewView: some View {
        VStack(spacing: 0) {
            Text("Overview")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text(viewModel.highSchoolData.schoolInfo.overview ?? "No overview found")
                .font(.body)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 5)
        }
    }
    
    var satScoreView: some View {
        VStack(spacing: 0) {
            Text("SAT Scores")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            Text("(R: Reading | W: Writing | M: Math)")
                .font(.caption)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 5)
            
            HStack(spacing: 10) {
                Card {
                    VStack(spacing: 10) {
                        Text("R")
                            .font(.largeTitle)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                        Text("\(viewModel.highSchoolData.satInfo.readingScore ?? 0)")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                    }
                }
                Card {
                    VStack(spacing: 10) {
                        Text("W")
                            .font(.largeTitle)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                        Text("\(viewModel.highSchoolData.satInfo.writingScore ?? 0)")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                    }
                }
                Card {
                    VStack(spacing: 10) {
                        Text("M")
                            .font(.largeTitle)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                        
                        Text("\(viewModel.highSchoolData.satInfo.mathScore ?? 0)")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .center)
                    }
                }
            }
            .padding(.top, 20)
        }
    }
    
    var locationView: some View {
        VStack(spacing: 0) {
            Text("Location")
                .font(.largeTitle)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            MapView(latitude: viewModel.highSchoolData.schoolInfo.latitude ?? 0.0, longitude: viewModel.highSchoolData.schoolInfo.longitude ?? 0.0)
                .frame(maxWidth: .infinity, idealHeight: 300)
                .cornerRadius(25)
                .padding(.top, 20)
            
            Button(action: { viewModel.openAppleMap() }) {
                HStack(spacing: 10) {
                    Image(systemName: "apple.logo")
                        .font(.body)
                        .tint(.white)
                    Text("Open in Maps")
                        .font(.body)
                        .foregroundColor(.white)
                }
                .frame(maxWidth: .infinity)
                .padding(15)
                .background(Capsule().stroke(Color.white, lineWidth: 1))
                .background(Color.clear)
            }
            .padding(.top, 10)
        }
    }
}

struct HighSchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        HighSchoolDetailView(viewModel: HighSchoolDetailView.ViewModel(highSchoolData: HighSchoolData.previewData()))
    }
}
