//
//  HighSchoolDetailViewModel.swift
//  NY School App
//
//  Created by Shubroto Shuvo on 8/11/23.
//

import Foundation
import Combine
import SwiftUI

extension HighSchoolDetailView {
    class ViewModel: ObservableObject {
        @Published var highSchoolData: HighSchoolData
        
        init(highSchoolData: HighSchoolData) {
            self.highSchoolData = highSchoolData
        }
        
        //TODO: - Move to cordinator or some deep link handler wrapper to handle all deeplinks
        func openAppleMap() {
            let urlString = "https://maps.apple.com/?ll=\(highSchoolData.schoolInfo.latitude ?? 0.0),\(highSchoolData.schoolInfo.longitude ?? 0.0)&dirflg=d"
            
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url)
            }
        }
        
        func openWebView() {
            guard let webURL = URL(string: highSchoolData.schoolInfo.website ?? "") else {
                print("Invalid URL")
                return
            }
            UIApplication.shared.open(webURL)
        }
    }
}
