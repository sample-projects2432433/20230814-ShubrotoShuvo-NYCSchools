//
//  HomeViewModelTests.swift
//  NY School AppTests
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation
import XCTest

@testable import NY_School_App

class HomeViewModelTests: XCTestCase {
    var subject: HomeViewModel!
    var mockHighSchoolAPI: MockHighSchoolAPI!
    
    override func setUp() {
        mockHighSchoolAPI = MockHighSchoolAPI()
        subject = HomeViewModel(highSchoolApi: mockHighSchoolAPI)
        super.setUp()
    }
    
    override func tearDown() {
        subject = nil
        mockHighSchoolAPI = nil
        super.tearDown()
    }
    
    func test_GivenTwoModels_whenGetHighSchoolDataCalled() {
        let expectation = XCTestExpectation(description: "Waiting for high school model")
        var cancellable = subject.highSchoolDataSubject
            .sink { error in
                XCTFail("Error should not be produced \(error)")
            } receiveValue: { models in
                XCTAssertEqual(2, models.count)
                expectation.fulfill()
            }
        
        mockHighSchoolAPI.models = [HighSchoolModel(dbn: "111111"), HighSchoolModel(dbn: "222222")]
        mockHighSchoolAPI.satScoremodels = [SatScoreModel(dbn: "111111"), SatScoreModel(dbn: "222222")]
        
        subject.getHighSchoolData()
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_GivenBadURLError_whenGetHighSchoolDataCalled_thenShowError() {
        let expectation = XCTestExpectation(description: "Waiting for high school model")
        var cancellable = subject.highSchoolDataSubject
            .sink { error in
                expectation.fulfill()
            } receiveValue: { models in
                XCTFail("Should show error here and not model")
            }
        mockHighSchoolAPI.showError = true
        subject.getHighSchoolData()
        wait(for: [expectation], timeout: 1.0)
    }
}

class MockHighSchoolAPI: HighSchoolAPI {
    var models: [HighSchoolModel] = []
    var showError = false
    func getSchoolList() async throws -> [HighSchoolModel] {
        if showError {
            throw URLError(.badURL)
        }
        return models
    }
    
    var satScoremodels: [SatScoreModel] = []
    var showSatScoreError = false
    func getSATScore() async throws -> [SatScoreModel] {
        if showSatScoreError {
            throw URLError(.badURL)
        }
        return satScoremodels
    }
    
}
