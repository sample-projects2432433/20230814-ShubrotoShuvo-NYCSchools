//
//  MockURLSession.swift
//  NY School AppTests
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation

@testable import NY_School_App

class MockURLSessionHelper: URLSessionHelper {
    var method: NY_School_App.Method?
    var url: String?
    var model: Codable.Type?
    
    var mockJsonData = """
    {
    
    }
    """.data(using: .utf8)!
    
    func send<T: Codable>(_ url: String, method: NY_School_App.Method, model: T.Type) async throws -> T {
        self.url = url
        self.method = method
        self.model = model
        
        return try JSONDecoder().decode(T.self, from: mockJsonData)
    }
    
    func setMockData(from src: String) {
        let testBundle = Bundle(for: type(of: self))
        if let url = testBundle.url(forResource: "\(src)", withExtension: "json") {
            do {
                mockJsonData = try Data(contentsOf: url)
            } catch {
                print("Error loading JSON file:", error)
            }
        } else {
            print("Could not find JSON file")
        }
    }
}
