//
//  HighSchoolAPITests.swift
//  NY School AppTests
//
//  Created by Shubroto Shuvo on 8/8/23.
//

import Foundation
import XCTest

@testable import NY_School_App

class HighSchoolAPITests: XCTestCase {
    var subject: HighSchoolAPIImp!
    var mockURLSessionHelper: MockURLSessionHelper!
    
    override func setUp() {
        super.setUp()
        mockURLSessionHelper = MockURLSessionHelper()
        subject = HighSchoolAPIImp(urlSession: mockURLSessionHelper)
    }
    
    override func tearDown() {
        subject = nil
        mockURLSessionHelper = nil
        super.tearDown()
    }
    
    func test_endpoint_whengetSchoolListCalled() async throws {
        mockURLSessionHelper.setMockData(from: "HighSchoolResponse")
        let _ = try await subject.getSchoolList()
        let expectedURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        XCTAssertEqual(expectedURL, mockURLSessionHelper.url)
    }
    
    func test_Method_whenGetSchoolListCalled() async throws {
        mockURLSessionHelper.setMockData(from: "HighSchoolResponse")
        let _ = try await subject.getSchoolList()
        let expectedMethod: NY_School_App.Method = .GET
        XCTAssertEqual(expectedMethod, mockURLSessionHelper.method)
    }
    
    // Test to make sure the JSON key is matching to Model Key
    func test_response_whenGetSchoolListCalled() async throws {
        mockURLSessionHelper.setMockData(from: "HighSchoolResponse")
        let results = try await subject.getSchoolList()
        
        XCTAssertEqual(1, results.count)
        let result = results[0]
        
        XCTAssertEqual("111111", result.dbn ?? "")
        XCTAssertEqual("Test School", result.name ?? "")
        XCTAssertEqual("Test Overview", result.overview ?? "")
        XCTAssertEqual("000-000-0000", result.phone ?? "")
        XCTAssertEqual("test@test.com", result.email ?? "")
        XCTAssertEqual("www.test.com", result.website ?? "")
        XCTAssertEqual("50", result.totalStudents ?? "")
        XCTAssertEqual("Test", result.city ?? "")
    }
    
    func test_endpoint_whenGetSATScoreCalled() async throws {
        mockURLSessionHelper.setMockData(from: "SatScoreResponse")
        let _ = try await subject.getSATScore()
        let expectedURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        XCTAssertEqual(expectedURL, mockURLSessionHelper.url)
    }
    
    func test_Method_whenGetSATScoreCalled() async throws {
        mockURLSessionHelper.setMockData(from: "SatScoreResponse")
        let _ = try await subject.getSATScore()
        let expectedMethod: NY_School_App.Method = .GET
        XCTAssertEqual(expectedMethod, mockURLSessionHelper.method)
    }
    
    // Test to make sure the JSON key is matching to Model Key
    func test_response_whenGetSATScoreCalled() async throws {
        mockURLSessionHelper.setMockData(from: "SatScoreResponse")
        let results = try await subject.getSATScore()
        
        XCTAssertEqual(1, results.count)
        let result = results[0]
        
        XCTAssertEqual("111111", result.dbn ?? "")
        XCTAssertEqual(50, result.satTakers ?? -1)
        XCTAssertEqual(400, result.mathScore ?? -1)
        XCTAssertEqual(200, result.writingScore ?? -1)
        XCTAssertEqual(300, result.readingScore ?? -1)
    }
    
}
